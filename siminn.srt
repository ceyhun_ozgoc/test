1
00:00:01,160 --> 00:00:02,280
<i>Úr fyrri þáttum...</i>

2
00:00:04,320 --> 00:00:06,000
Renko! Renko, vertu hjá mér.
Ekki gera mér þetta.

3
00:00:06,240 --> 00:00:07,800
Hann vill að við finnum hann.
–Hver?

4
00:00:07,880 --> 00:00:11,920
Kameljónið.
–Ég þarf að svara þessu.

5
00:00:12,000 --> 00:00:14,480
Hjálpaðu mér!

6
00:00:37,520 --> 00:00:39,800
Ertu reið?

7
00:00:44,200 --> 00:00:50,080
Ekki reið. –Ég verð að viðurkenna 
að ég er meira en reið.

8
00:00:50,160 --> 00:00:52,400
Þetta átti ekki að koma fyrir mig.
–Ég lofaði þér aldrei

9
00:00:52,480 --> 00:00:56,440
að það gerðist ekki.
–Það gerir þig ekki saklausa.

10
00:00:56,520 --> 00:00:59,680
Nei, það gerir það ekki.
–Það gefur þér samt eitthvað

11
00:00:59,760 --> 00:01:06,200
til að halda í.
–Sleipan stein á flóði.

12
00:01:08,120 --> 00:01:14,160
Ef ekki reið, hvað þá?
–Mér finnst ég vera að drukkna.

13
00:01:14,880 --> 00:01:20,440
Það er skárra en að vera
sprengd í loft upp. Hver er ég?

14
00:01:20,520 --> 00:01:24,160
Sú fjórða. Það eru fleiri
hinum megin auðvitað.

15
00:01:24,200 --> 00:01:27,000
Þú gætir staflað líkunum upp
eins og viði.

16
00:01:27,040 --> 00:01:34,440
Hver var þriðji?
–Renko. Annar var Dorn

17
00:01:34,480 --> 00:01:40,280
og Sullivan var sá fyrsti.
–Talarðu líka við þá?

18
00:01:41,680 --> 00:01:44,840
Við og við.

19
00:01:50,640 --> 00:01:54,520
Heldurðu að ég sé klikkuð?
–Sennilega. 

20
00:01:54,640 --> 00:01:58,240
Manstu þegar þú náðir í mig 
á munaðarleysingjahælið?

21
00:01:58,360 --> 00:02:02,880
Ég sleppti hendinni á þér ekki
í þrjá daga. Ég var sjö ára og...

22
00:02:03,000 --> 00:02:07,760
Hættu þessu!
–Of snemmt fyrir góðar minningar?

23
00:02:07,840 --> 00:02:13,760
Þú varst alltaf ósvífinn krakkaormur.
–Þess vegna elskaðirðu mig.

24
00:02:14,360 --> 00:02:17,400
Of mikið.

25
00:02:28,840 --> 00:02:34,480
Hann talar ekki.
Andskotinn sem gerði þetta.

26
00:02:34,560 --> 00:02:37,320
Er engin útskýring
á hegðun hans?

27
00:02:37,440 --> 00:02:40,280
Nei, það er ekkert vit í honum.
Hann leiðir okkur þangað,

28
00:02:40,360 --> 00:02:46,080
drepur Hunter og gefst upp.
Hann hefur ekki sagt orð.

29
00:02:47,560 --> 00:02:51,080
Ég ætla að láta Callen fá hann.

30
00:02:51,920 --> 00:02:54,440
Hvað? 
Telurðu það vera mistök?

31
00:02:54,520 --> 00:02:58,640
Þetta allt saman eru mistök, Owen.

32
00:03:04,680 --> 00:03:10,120
Við hvern varstu að tala?
–Þá dauðu.

33
00:03:41,120 --> 00:03:44,560
Byrjum á fullu nafni.

34
00:03:48,200 --> 00:03:52,120
Allt í lagi, hvað með fornafn?

35
00:03:56,120 --> 00:04:00,240
Leyfðu mér að giska.
Þú vilt lögfræðing.

36
00:04:02,680 --> 00:04:05,800
Það hljómar eins og hann
vilji ekki lögfræðing, Sam.

37
00:04:05,880 --> 00:04:10,640
Það er í lagi mín vegna.
–Hvað viltu?

38
00:04:12,360 --> 00:04:16,640
Réttinn til að vera þögull?
Það er í lagi, þú færð hann.

39
00:04:16,720 --> 00:04:22,760
Það mun ekki hjálpa þér
því við vitum að fyrr eða síðar

40
00:04:22,800 --> 00:04:27,880
muntu tala við mig.
Því þetta snýst um það, ekki satt?

41
00:04:28,000 --> 00:04:32,400
Mig og þig.
–Hefurðu smástund?

42
00:04:38,960 --> 00:04:44,920
Hvað er klukkan? Klukkan.
–Tíu mínútur yfir sex.

43
00:04:52,160 --> 00:04:58,040
Eftir nákvæmlega sex tíma
geng ég héðan út, Callen,

44
00:05:02,040 --> 00:05:06,960
og þú heldur hurðinni fyrir mig.

45
00:05:19,160 --> 00:05:23,000
Callen fulltrúi, fyrir sex vikum
hvarf sérfræðingur frá NSA,

46
00:05:23,080 --> 00:05:27,880
Kelvin Atley. Eftir hvarf hans
komst NSA að því að hann hefði

47
00:05:27,960 --> 00:05:30,600
notað heimild sína til að komast
inn í háleynilega skrá

48
00:05:30,680 --> 00:05:34,480
sem nefnd var Cherokee.
Nell.

49
00:05:34,600 --> 00:05:39,400
Cherokee er leyninafn uppljóstrara
í hæstu þrepum Íranshers.

50
00:05:39,480 --> 00:05:43,280
Cherokee hefur sérstaklega gefið
gagnlegar upplýsingar

51
00:05:43,360 --> 00:05:46,160
um kjarnorkuframleiðslu Írans.
–Atley veit allt um Cherokee,

52
00:05:46,240 --> 00:05:49,120
þ. á m. nafnið hans.
Hunter fulltrúi fékk nafnlausa

53
00:05:49,200 --> 00:05:52,800
ábendingu um að Atley væri í París,
elti hann til New York 

54
00:05:52,880 --> 00:05:55,120
fyrir tveimur dögum og hvarf.
–Heldurðu að ábendingin

55
00:05:55,200 --> 00:05:56,880
hafi komið frá Kameljóninu?
–Það virðist vera.

56
00:05:57,000 --> 00:05:58,600
Hvernig vitum við þá að hann
hafi Atley? 

57
00:05:58,680 --> 00:06:00,360
Við vitum það ekki en ef hann
hefur hann þurfum við að ná honum

58
00:06:00,440 --> 00:06:03,000
til baka áður en hann
leggur mann okkar í Teheran í hættu.

59
00:06:03,040 --> 00:06:05,080
Hvar kemur Renko inn í þetta?
–Við sjáum ekki betur en að hann

60
00:06:05,160 --> 00:06:08,920
geri það ekki. Ég veit ekki
af hverju hann drap Renko.

61
00:06:08,960 --> 00:06:13,640
Kannski var hann bara að reyna
að ná athygli okkar.

62
00:06:16,720 --> 00:06:20,800
Af hverju segirðu okkur ekki
frá Kelvin Atley?

63
00:06:20,920 --> 00:06:24,360
Ég myndi frekar vilja segja ykkur
frá Mayfield–fjölskyldunni.

64
00:06:24,400 --> 00:06:29,320
Byssufjölskyldunni?
–Ólokið mál frá því fyrir fimm árum.

65
00:06:29,400 --> 00:06:35,920
Það voru mistök að treysta þeim.
Takk fyrir að sjá um það fyrir mig.

66
00:06:36,600 --> 00:06:42,120
Þú leiddir þá í gildru.
–Bara nóg til að Renko fengi áhuga.

67
00:06:43,000 --> 00:06:46,040
Ég hélt þeir myndu komast að því
að hann væri fulltrúi NCIS

68
00:06:46,160 --> 00:06:51,240
en þeir gerðu það ekki.
–Svo þú lést þá vita.

69
00:06:51,280 --> 00:06:56,200
Og lést okkur svo vita
að auðkenni Renkos væri í hættu.

70
00:06:59,800 --> 00:07:02,920
Að setja upp taflið.

71
00:07:03,000 --> 00:07:08,440
Af hverju skaustu hann þá?
–Fyrsta hreyfing.

72
00:07:09,440 --> 00:07:13,960
Þið svöruðuð með því
að eltast við mig.

73
00:07:14,040 --> 00:07:19,240
Önnur hreyfing.
Ég drap Hunter fulltrúa

74
00:07:21,640 --> 00:07:25,120
og þið svöruðuð með því
að færa mig hingað.

75
00:07:25,200 --> 00:07:29,840
Er þetta leikur fyrir þér?
–Teflirðu, Callen fulltrúi?

76
00:07:30,800 --> 00:07:36,120
Aðeins.
–Hugtakið <i>sans voir.</i>

77
00:07:37,360 --> 00:07:40,760
Að tefla með bundið fyrir augun.

78
00:07:45,680 --> 00:07:51,200
Þú ert í öruggu rými.
Hver er sá blindi hérna?

79
00:07:52,440 --> 00:07:56,160
Þú ert það.
–Í alvöru? 

80
00:07:56,240 --> 00:08:00,400
Og hver á að gera?
–Ég.

81
00:08:18,440 --> 00:08:25,240
Vefsíða til að deila skjölum?
–Lykilorðið er Muley Hassan.

82
00:08:34,680 --> 00:08:38,000
Þetta er opinber vefsíða
þar sem maður hleður upp skjölum

83
00:08:38,080 --> 00:08:40,680
og hver sá sem er með lykilorðið
getur halað þau niður.

84
00:08:40,760 --> 00:08:44,560
Merkir Muley Hassan eitthvað
fyrir einhvern hérna?

85
00:08:44,640 --> 00:08:48,360
Hann var síðasti emírinn
í Granada á 15. öld.

86
00:08:48,440 --> 00:08:51,640
Hann tefldi ekki, er það?
–Hann var frægur fyrir það.

87
00:08:51,760 --> 00:08:54,960
Hann notaði fanga úr konunglegu
dýflissunum sem peð

88
00:08:55,040 --> 00:09:00,320
og ef einn þeirra náðist lét hann
afhöfða fangann á staðnum.

89
00:09:01,320 --> 00:09:06,080
Skákklúbburinn.
–Niðurhalinu er lokið.

90
00:09:06,160 --> 00:09:09,280
Þetta er myndband.

91
00:09:16,240 --> 00:09:19,000
Spilaðu það, Eric.

92
00:09:19,040 --> 00:09:23,840
Ég klúðraði þessu.
Fyrirgefið að ég brást ykkur.

93
00:09:23,920 --> 00:09:28,840
Svona nú, segðu þeim það.
–Hann vill að ég segi ykkur

94
00:09:28,920 --> 00:09:32,200
að hann heiti Marcel Janvier.
Hann er franskur ríkisborgari,

95
00:09:32,280 --> 00:09:37,040
fæddur í Lyon.
Hann er með Kelvin Atley sem fanga.

96
00:09:37,080 --> 00:09:38,920
Ef þið veitið honum ekki það 
sem hann vill mun hann setja Atley

97
00:09:39,000 --> 00:09:41,920
á markaðinn og selja hann
hæstbjóðanda.

98
00:09:42,000 --> 00:09:45,000
Segðu þeim hvað ég þarf.
–Hann vill 50 milljónir dala

99
00:09:45,080 --> 00:09:48,320
inn á bankareikninginn sinn
fyrir hádegi í dag.

100
00:09:48,400 --> 00:09:52,000
Hann gefur ykkur reikningsnúmerið.

101
00:09:57,720 --> 00:10:01,800
Láttu greina þetta myndband, Eric.
Þekkir einhver tungumálið?

102
00:10:01,880 --> 00:10:03,840
Þetta var sígaunamál.
Þetta var tala.

103
00:10:03,920 --> 00:10:06,280
Eins og símanúmer.
–Hvernig heldur hann að hann

104
00:10:06,360 --> 00:10:10,200
geti drepið einhvern 
fyrir framan okkur og svo búist við

105
00:10:10,280 --> 00:10:15,520
að við sleppum honum?
–Því Janvier, ef hann heitir það,

106
00:10:15,600 --> 00:10:18,000
heldur að við höfum ekkert val.

107
00:10:18,080 --> 00:10:22,560
Janvier fyrir Atley.
Hann vill fangaskipti.

108
00:10:31,960 --> 00:10:34,040
Númerið er í gemsa.
–Með GPS?

109
00:10:34,120 --> 00:10:36,040
Það er slökkt á því.
Þetta er einnota sími og númerið

110
00:10:36,120 --> 00:10:38,440
var fyrst notað fyrir þremur dögum.
–Finndu út hvar hann var keyptur.

111
00:10:38,520 --> 00:10:40,000
Kannski við verðum heppin
með myndband úr búðinni

112
00:10:40,080 --> 00:10:45,120
eða kreditkortanótu.
<i>–Hetty, við sleppum honum ekki.</i>

113
00:10:45,440 --> 00:10:48,880
Nei, við gerum það ekki.

114
00:10:48,960 --> 00:10:53,960
Það er ekki ákvörðum sem við
fáum að taka, Callen fulltrúi.

115
00:10:54,840 --> 00:10:59,080
Stundum hata ég þennan mann.
–Bara stundum?

116
00:10:59,160 --> 00:11:13,040
Hvernig náðirðu Atley?
–Eins og Hunter, Renko og þér,

117
00:11:13,080 --> 00:11:17,040
Callen fulltrúi.
Það hafa allir sína veikleika.

118
00:11:18,000 --> 00:11:21,640
Hver heldurðu að vilji Atley mest?
Kínverjarnir? 

119
00:11:21,720 --> 00:11:24,920
Ísraelarnir?
–Þeir eru sannarlega áhugaverðir.

120
00:11:24,960 --> 00:11:27,560
Þeir verða með í keppninni
en ég held það sé til fólk sem vill

121
00:11:27,640 --> 00:11:32,560
Atley meira en Ísraelarnir.
Íranarnir.

122
00:11:32,640 --> 00:11:37,960
Þeir eru með svikara innanhúss.
Atley veit hver það er.

123
00:11:38,040 --> 00:11:41,840
Teheran.
Þar fæst besta verðið.

124
00:11:41,880 --> 00:11:46,840
Svo ef við kaupum hann ekki
gera Íranarnir það.

125
00:11:48,560 --> 00:11:52,160
Rændirðu honum?
–Kúgaði hann.

126
00:11:52,280 --> 00:11:57,640
Með konu? –Stelpu.
Undir lögaldri.

127
00:11:57,720 --> 00:12:03,280
Hann trúði mér alveg þegar ég
sagði að við yrðum félagar.

128
00:12:04,880 --> 00:12:10,440
Það er góð tilraun en Atley
er hommi. Hann hélt hann hefði

129
00:12:10,520 --> 00:12:15,920
haldið því leyndu fyrir NSA
en það stendur í skýrslunni hans.

130
00:12:16,000 --> 00:12:20,720
Velkominn í leikinn, Callen fulltrúi.

131
00:12:24,800 --> 00:12:27,720
Hvað ertu með, Eric?
<i>–Búið er að kveikja á gemsanum.</i>

132
00:12:27,760 --> 00:12:30,280
GPS–staðsetningin staðsetur hann
í yfirgefnu vöruhúsi niðri í bæ.

133
00:12:30,360 --> 00:12:32,200
Heimilisfangið er
að hlaðast upp núna.

134
00:12:32,240 --> 00:12:34,760
Af hverju eyddi Janvier
því sem hún sagði ekki út?

135
00:12:34,840 --> 00:12:37,520
Annaðhvort er honum sama eða hann 
vildi að við eltumst við það.

136
00:12:37,600 --> 00:12:40,200
Eins og við höfum val.
–Ekki missa merkið, Eric.

137
00:12:40,280 --> 00:12:42,560
<i>Ekki þú, Callen fulltrúi.</i>
<i>Ég vil að þú haldir pressu</i>

138
00:12:42,600 --> 00:12:47,200
<i>á Janvier.</i>
Ekki reyna á þolinmæði mína.

139
00:13:33,360 --> 00:13:37,800
Það er ekkert fólk hér.
Þetta er eins og draugabær.

140
00:13:38,200 --> 00:13:42,880
Ég held mér líki þetta ekki.
–Ekki mér heldur.

141
00:13:51,440 --> 00:13:56,880
Þú slepptir mér einu sinni.
–Ég vissi ekki hver þú varst.

142
00:13:56,960 --> 00:14:03,480
En núna veistu það.
Og þú þarft samt að sleppa mér.

143
00:14:04,960 --> 00:14:08,440
Það gerist ekki.

144
00:14:09,040 --> 00:14:11,840
Myndin er komin upp.

145
00:14:13,240 --> 00:14:16,680
Ég fer aftur fyrir.
–Náði því.

146
00:14:16,880 --> 00:14:22,520
Hvernig ertu í munninum?
–Ég fæ enn höfuðverk.

147
00:14:22,640 --> 00:14:27,920
Gott.
–Mér finnst sjávarloftið hjálpa.

148
00:14:30,280 --> 00:14:34,240
Þú gerðir mistök
daginn sem ég skaut þig.

149
00:14:34,280 --> 00:14:38,280
Það gerist ekki aftur.
–Ertu viss um það?

150
00:14:38,320 --> 00:14:40,840
Ert þú það?

151
00:14:46,680 --> 00:14:48,880
Kens.

152
00:14:58,360 --> 00:15:03,960
Stundum þarf maður að fórna peði
til að ná forskotinu.

153
00:15:04,360 --> 00:15:09,880
Maður þarf að vita næstu hreyfingu
andstæðingsins á undan honum.

154
00:15:13,880 --> 00:15:17,280
<i>Eric! Komdu þeim þaðan út!</i>
<i>Þetta er gildra!</i>

155
00:15:17,360 --> 00:15:20,320
Komið ykkur út úr byggingunni!
–Áfram! Áfram!

156
00:15:54,360 --> 00:15:58,960
Er allt í lagi með þig?
–Já, en þig?

157
00:16:02,120 --> 00:16:08,640
Sam! Sam?
–Sam!

158
00:16:13,240 --> 00:16:15,520
Sam!

159
00:16:48,520 --> 00:16:52,800
Ef þú vilt drepa einhvern annan
skaltu hafa það mig. Hérna.

160
00:16:52,880 --> 00:16:57,040
Notaðu þetta.
Þú þarft bara að taka í gikkinn.

161
00:16:57,120 --> 00:17:01,600
Taktu þetta!
Taktu þetta!

162
00:17:02,040 --> 00:17:06,480
Og gefa þér afsökun fyrir
að hálsbrjóta mig?

163
00:17:07,000 --> 00:17:13,360
Ég þarf ekki aðra afsökun.
–Þú metur líf Hunters og Renkos

164
00:17:13,440 --> 00:17:17,360
minna en hinna.

165
00:17:21,680 --> 00:17:26,800
Ef þú drepur mig þá vinn ég.

166
00:17:29,200 --> 00:17:32,120
Hættu, herra Callen.

167
00:17:33,920 --> 00:17:37,720
Ég sagði þér að hætta.

168
00:17:42,360 --> 00:17:46,680
Getum við búist við að þú
talir rómamálið?

169
00:17:46,760 --> 00:17:51,200
Ekki jafn vel og þú.

170
00:17:55,640 --> 00:18:01,720
Dó einhver? –Nei.
–En leiðinlegt.

171
00:18:14,240 --> 00:18:19,880
Leggðu þetta frá þér áður en ég
tek hana af þér og skýt hann sjálf.

172
00:18:21,640 --> 00:18:25,560
Hann talaði rómamál, ekki satt?
–Hann vissi hvað Hunter sagði

173
00:18:25,640 --> 00:18:27,920
um leið og hún sagði það.
–Svo hann kom númerinu fyrir,

174
00:18:27,960 --> 00:18:30,280
lét hana telja það mikilvægt og notaði
hana svo til að leiða okkur í gildru.

175
00:18:30,360 --> 00:18:37,400
Já.
Síðast þegar ég missti fulltrúa

176
00:18:37,480 --> 00:18:42,840
afhenti ég Vance deildarstjóra
uppsagnarbréfið mitt.

177
00:18:42,920 --> 00:18:45,720
Og ég stal því úr vasanum hans
og lét þig fá það aftur.

178
00:18:45,760 --> 00:18:51,920
Það er enn í skrifborðinu mínu
en nú missti ég tvo fulltrúa.

179
00:18:52,240 --> 00:18:57,840
Var hún sérstök?
–Þú veist ég á ekki uppáhald.

180
00:18:59,160 --> 00:19:04,040
Ertu að segja mér
að eftir öll þessi ár...

181
00:19:10,520 --> 00:19:15,640
Gastu séð framan í hana í lokin?

182
00:19:25,040 --> 00:19:27,720
Vissi hún það? 

183
00:19:32,040 --> 00:19:35,160
Var hún hrædd?

184
00:19:40,000 --> 00:19:43,400
Reið og ögrandi.

185
00:19:46,280 --> 00:19:50,800
Já, hún var hrædd.

186
00:20:00,000 --> 00:20:05,600
Hversu lengi þekktirðu hana?
–Í næstum 30 ár.

187
00:20:06,160 --> 00:20:09,840
Ekki jafnlengi og þig
en eins og þig fann ég hana

188
00:20:09,920 --> 00:20:13,680
á munaðarleysingjaheimili.

189
00:20:15,440 --> 00:20:20,720
Eru einhverjir aðrir munaðarlausir
krakkar sem þú vilt segja mér frá?

190
00:20:23,440 --> 00:20:28,000
Hún sagði eitthvað annað í lokin,
er það ekki? Á spólunni

191
00:20:28,040 --> 00:20:39,880
þegar hún gaf upp númerið.
–Fyrirgefðu. Hún sagði það.

192
00:20:43,040 --> 00:20:46,800
Hún var að segja það við mig.

193
00:20:52,480 --> 00:20:55,880
Við hleypum honum ekki héðan.

194
00:20:56,840 --> 00:21:01,560
Við leyfum þeim ekki að skipta
á þessum gaur og Atley.

195
00:21:01,640 --> 00:21:06,200
Og ef við gerum það ekki og Atley
deilir leyndarmálum sínum

196
00:21:06,240 --> 00:21:10,360
mun mjög hugrakkur maður í Teheran
örugglega deyja.

197
00:21:10,400 --> 00:21:13,840
Heldurðu að við höfum val, Callen?

198
00:21:15,000 --> 00:21:19,280
Þú hefðir átt að leyfa mér
að skjóta hann.

199
00:21:26,760 --> 00:21:29,440
Umferðarmyndavélar. 

200
00:21:31,240 --> 00:21:33,880
Ein?
Ein umferðarmyndavél. Í alvöru?

201
00:21:33,960 --> 00:21:37,520
Þetta er myndavél í rauðu ljósi
einni götu frá vöruhúsinu.

202
00:21:37,600 --> 00:21:41,960
Reynum að ná upp myndum
strax eftir að sprengjan sprakk.

203
00:21:43,120 --> 00:21:45,960
Bíddu. Farðu til baka.

204
00:21:46,680 --> 00:21:49,240
Þessi gaur er að flýta sér.
Kannski við höfum orðið heppin.

205
00:21:49,280 --> 00:21:53,080
Settu smáatriðin í Kaleidoscope.
Athugum hvert hann fór. –Já.

206
00:21:53,120 --> 00:21:54,880
Sam.
–Já.

207
00:21:54,920 --> 00:21:56,920
<i>Ég er með dálítið handa þér.</i>
–Hvað er það, Eric?

208
00:21:56,960 --> 00:21:59,800
<i>Það fór bíll yfir á rauðu ljósi</i>
<i>í enda götunnar tæpum 30 sekúndum</i>

209
00:21:59,880 --> 00:22:03,520
<i>eftir að sprengjan sprakk.</i>
<i>Svartur Mercedes af M–gerðinni.</i>

210
00:22:03,600 --> 00:22:08,520
Númerið er 2SA...
–Q321?

211
00:22:08,600 --> 00:22:13,480
Hvernig vissirðu það?
–Því ég stari á það núna.

212
00:22:13,560 --> 00:22:15,640
Eric, þú vilt kannski finna
hver á þetta bílnúmer.

213
00:22:15,720 --> 00:22:19,320
Ég fer í málið.
–Klukkan tólf.

214
00:22:19,400 --> 00:22:23,160
Nálægt krakkanum með hjólabrettið.
–Kannski.

215
00:22:23,240 --> 00:22:26,280
Gæti verið.

216
00:22:27,920 --> 00:22:31,640
Þetta eru þeir.
–Af stað.

217
00:23:47,040 --> 00:23:49,240
Það er risastór lest
vinstra megin við þig.

218
00:23:49,280 --> 00:23:52,240
Þegiðu, Deeks.

219
00:24:00,400 --> 00:24:03,960
Þú nærð því ekki!
Þú nærð því ekki!

220
00:24:11,360 --> 00:24:13,760
Alríkisfulltrúar!
–Út úr bílnum og setjið hendurnar

221
00:24:13,800 --> 00:24:17,200
þar sem við getum séð þær.

222
00:24:20,320 --> 00:24:24,120
Farið upp að lestinni.
Leggið hendurnar á lestina.

223
00:24:24,200 --> 00:24:28,840
Af stað!
–Bíllinn er skráður á...

224
00:24:32,960 --> 00:24:36,040
Þetta er ekki gott.

225
00:24:36,560 --> 00:24:43,480
Já, Eric. <i>–Sam, ég er með</i>
<i>slæmar fréttir fyrir þig.</i>

226
00:24:43,560 --> 00:24:47,760
Horfðu fram.
–Ha? 

227
00:24:49,400 --> 00:24:52,840
Ekki mistök?
–Ekki mistök. 

228
00:24:52,880 --> 00:24:56,320
Bíllinn er skráður
á ísraelska sendiráðið.

229
00:24:56,400 --> 00:25:00,600
<i>Sam, þú þarft að sleppa þeim.</i>

230
00:25:10,080 --> 00:25:12,600
Þeir voru í vöruhúsinu.
Þeir reyndu að komast í burtu.

231
00:25:12,640 --> 00:25:15,600
Ef þeir áttu ekki þátt í þessu
gætu þeir a.m.k. hafa séð eitthvað.

232
00:25:15,680 --> 00:25:18,480
<i>Ég skil það, Hanna fulltrúi,</i>
<i>en ég er að segja þér að sleppa þeim.</i>

233
00:25:18,560 --> 00:25:20,840
Utanríkisráðuneytið hefur staðfest
auðkenni þeirra og þeir vinna báðir

234
00:25:20,920 --> 00:25:24,480
í ísraelska sendiráðinu.
–Ísraelska leyniþjónustan?

235
00:25:24,520 --> 00:25:31,200
Sennilega. Við þurfum að sleppa þeim.
Slepptu þeim. Það er skipun.

236
00:25:32,840 --> 00:25:35,280
Já, herra. 

237
00:25:38,240 --> 00:25:41,520
Þið verðið kærðir
fyrir morðtilraun á alríkisfulltrúa.

238
00:25:41,560 --> 00:25:44,160
Þú þarft að tala við utanríkis–
ráðuneytið. –Við gerðum það.

239
00:25:44,240 --> 00:25:45,960
Þetta er hneyksli.
–Tveir alríkisfulltrúar eru látnir

240
00:25:46,040 --> 00:25:48,120
og bandamenn okkar neita að segja
okkur hvað sé í gangi.

241
00:25:48,160 --> 00:25:51,760
Ekki tala við mig um hneyksli.
–Þetta fer aldrei fyrir rétt.

242
00:25:51,840 --> 00:25:56,200
Sennilega ekki en við getum dregið
þetta á langinn í 3–4 mánuði.

243
00:25:56,280 --> 00:25:58,120
Fundið handa ykkur rólegt horn
í helvíti svo þið munuð grátbiðja

244
00:25:58,200 --> 00:26:00,320
um að verða sendir til Gitmo.
–Ég fullvissa þig um að við höfðum

245
00:26:00,360 --> 00:26:02,480
enga hugmynd um hvað myndi gerast.
Ef við hefðum vitað það

246
00:26:02,520 --> 00:26:04,440
hefðum við varað ykkur við.
–Hvað voruð þið að gera þarna?

247
00:26:04,480 --> 00:26:07,160
Það sama og þið.
Að leita að manneskju.

248
00:26:07,240 --> 00:26:12,960
Hverjum?
Handjárnaðu þá.

249
00:26:13,040 --> 00:26:15,800
Ekkert mál. 

250
00:26:15,880 --> 00:26:19,320
Ég mun gera þetta
eins óþægilegt og ég get.

251
00:26:19,400 --> 00:26:23,040
Hann er franskur ríkisborgari.
Við misstum af honum í gærmorgun.

252
00:26:23,120 --> 00:26:25,920
Við sáum hann síðast í vöruhúsinu.
–Svo þið komuð til baka

253
00:26:25,960 --> 00:26:28,400
og fylgdust með því.
–Við höfðum ekki hugmynd um

254
00:26:28,480 --> 00:26:31,080
að byggingin væri hættuleg.
Við komum okkur fljótt í burtu

255
00:26:31,120 --> 00:26:32,920
og komum til baka til að reyna
að finna út eins mikið og við gætum.

256
00:26:33,000 --> 00:26:37,920
Og svo sáuð þið okkur.
–Rólegur.

257
00:26:38,000 --> 00:26:39,880
Ef þið eruð með fleiri spurningar
skuluð þið vinsamlegast

258
00:26:39,960 --> 00:26:42,240
senda þær inn skriflega
í sendiráðið okkar.

259
00:26:42,280 --> 00:26:45,480
Hafið það gott í dag.
–Náðirðu því, Deeks?

260
00:26:45,560 --> 00:26:47,480
Já, ég náði því.
Þeir verða svo pirraðir

261
00:26:47,520 --> 00:26:50,720
þegar þeir skoða myndavélina. 
–Gott.

262
00:26:53,000 --> 00:26:57,880
Vil ég vita hvaðan þú fékkst þetta?
–Sennilega ekki.

263
00:26:59,640 --> 00:27:03,560
Ég tel sex menn.
<i>–Janvier er með nýtt lið.</i>

264
00:27:03,640 --> 00:27:08,880
<i>Finnið þá og við finnum Atley.</i>
–Hver er dularfulli maðurinn okkar?

265
00:27:08,960 --> 00:27:11,240
Við höfum enga hreina mynd af honum.
Hann er annaðhvort dulinn

266
00:27:11,360 --> 00:27:13,480
eða að horfa í ranga átt
á réttum tíma.

267
00:27:13,560 --> 00:27:15,200
Myndirnar eru skráðar
með dagsetningu og tímasetningu

268
00:27:15,280 --> 00:27:17,240
og bryggjan í Santa Monica
er vinsæll ferðamannastaður.

269
00:27:17,280 --> 00:27:20,160
Margt fólk með margar myndavélar.
Við förum í gegnum síður

270
00:27:20,240 --> 00:27:22,360
samfélagsmiðlanna og athugum hvort
við getum sett saman yfirlitsmynd.

271
00:27:22,400 --> 00:27:25,000
Janvier er vel verndaður.
Þetta eru greinilega atvinnumenn.

272
00:27:25,040 --> 00:27:28,120
Hvað með bílana þeirra?
–Bara hlutar af bílnúmerum.

273
00:27:28,200 --> 00:27:30,600
Ég gæti hafið leit í Kaleidoscope.
–Þrengdu leitarmöguleikana.

274
00:27:30,640 --> 00:27:32,520
Í stað þess að leita 
að einstaka bílum skaltu leita

275
00:27:32,600 --> 00:27:34,920
að þeim öllum á sama stað,
t.d. fyrir utan hús eða á bílastæði.

276
00:27:34,960 --> 00:27:38,480
Það væri best ef við gætum
þrengt leitarsvæðið.

277
00:27:38,560 --> 00:27:43,360
<i>Nell, byrjaður að leita á ströndinni</i>
<i>við Malibu og í suður.</i>

278
00:27:43,440 --> 00:27:46,920
<i>Janvier er hrifinn</i>
<i>af sjávarloftinu.</i> –Geri það.

279
00:28:11,960 --> 00:28:18,440
G. Callen fulltrúi.
Viltu vita fyrir hvað G stendur?

280
00:28:20,960 --> 00:28:29,520
Auðvitað viltu það.
Gary. Gaston. Gustav.

281
00:28:32,280 --> 00:28:37,400
Ég hef svo mörg nöfn
og þú vilt bara eitt.

282
00:28:37,480 --> 00:28:42,440
Marcel Janvier.
Eitt af mörgum? 

283
00:28:45,240 --> 00:28:48,360
Af hverju ég?
–Þú skaust mig.

284
00:28:48,440 --> 00:28:51,000
En þú komst til baka.
–Forvitni.

285
00:28:51,040 --> 00:28:53,440
Ég náði þér næstum.
– En þú gerðir það ekki.

286
00:28:53,480 --> 00:28:58,840
Og ekki halda að þú hafir
náð mér í þetta skiptið.

287
00:29:03,120 --> 00:29:07,000
Hver á að gera? 
–Ég.

288
00:29:08,440 --> 00:29:12,480
Veldu varlega.
Þú átt ekki mikið eftir.

289
00:29:15,880 --> 00:29:21,880
Svona nú. Ég fann það.
Allir þrír bílarnir á sama stað.

290
00:29:21,920 --> 00:29:25,040
Þetta virðist vera íbúð
á Marina Del Rey.

291
00:29:25,080 --> 00:29:29,720
Við förum með lið þangað.
Hringdu í Callen. Af stað.

292
00:29:42,160 --> 00:29:45,560
Bílarnir þeirra eru enn hér.
–Vitum við hvort þeir séu það?

293
00:29:45,640 --> 00:29:47,920
Eigandi íbúðanna þekkti
Kelvin Atley af mynd.

294
00:29:47,960 --> 00:29:50,160
Þakíbúðin á 18. hæð
er í leigu í styttri tíma.

295
00:29:50,200 --> 00:29:54,280
Atley og sjö aðrir.
–Ef við náum Atley er þessu lokið.

296
00:29:54,360 --> 00:29:56,640
Við leikum okkar leik, G,
ekki hans.

297
00:29:56,680 --> 00:29:59,680
Hvort sem er þá tapar hann.
–Deeks. –Já.

298
00:29:59,760 --> 00:30:04,280
Lyklar að hurðinni.
–Ekkert mál.

299
00:30:36,560 --> 00:30:41,040
Það er allt öruggt.
Engir vírar. Ekkert springur.

300
00:31:01,280 --> 00:31:03,920
Autt.
–Autt.

301
00:31:04,240 --> 00:31:06,080
Autt.
–Autt.

302
00:31:06,120 --> 00:31:08,360
Það virðist sem einhver
hafi orðið á undan okkur.

303
00:31:08,400 --> 00:31:10,080
Atley?
–Nei. –Nei.

304
00:31:10,160 --> 00:31:14,080
Hann er ekki hér.
Hann er farinn.

305
00:31:14,320 --> 00:31:17,800
Strákar?
Hérna inni.

306
00:31:23,720 --> 00:31:27,160
SKÁK

307
00:31:37,640 --> 00:31:40,680
Já, herra, ég skil það.

308
00:31:40,720 --> 00:31:45,800
Nei, ég hef ekki hugmynd um
hvernig þetta gerðist.

309
00:31:46,080 --> 00:31:49,440
Ég set það í forgang, herra.

310
00:31:53,600 --> 00:31:56,360
Ísraelarnir eru reiðir.
–Ráðuneytisstjórinn 

311
00:31:56,440 --> 00:32:01,520
hjá utanríkisráðuneytinu vill rannsókn.
Hann spurði mig líka um týnt kort

312
00:32:01,640 --> 00:32:06,920
úr myndavél. Ég sagði honum
að við vissum ekkert um það.

313
00:32:07,000 --> 00:32:12,800
Hefurðu átt við einhvern
svona áður? –Nei.

314
00:32:13,000 --> 00:32:17,000
Hann er illur.
–Hann er margt

315
00:32:17,160 --> 00:32:24,600
en það er bara eitt sem hann er ekki
en ætti að vera. Dauður.

316
00:32:32,880 --> 00:32:37,040
Við höfum ekki meiri tíma.
Skiptin hafa verið samþykkt.

317
00:32:37,120 --> 00:32:39,520
Við færum Janvier það sem hann vill
og sleppum honum.

318
00:32:39,600 --> 00:32:44,200
Við fáum Atley og auðkenni
uppljóstrara okkar í Íran er öruggt.

319
00:32:44,280 --> 00:32:48,640
Ætlarðu að láta þetta gerast?
–Við gerum það öll, Callen.

320
00:32:48,680 --> 00:32:51,320
Svo Hunter og Renko
létust til einskis?

321
00:32:51,360 --> 00:32:53,640
Þau létust við að vernda
uppljóstrara sem gæti verið sá eini

322
00:32:53,720 --> 00:32:56,160
sem stendur í vegi fyrir
öðru stríði í Miðausturlöndum.

323
00:32:56,200 --> 00:32:58,360
Það verður alltaf annað stríð
í Miðausturlöndunum!

324
00:32:58,440 --> 00:33:00,440
<i>Afsakið truflunina.</i>
–Hvað viltu, Eric?

325
00:33:00,520 --> 00:33:02,360
<i>Við bárum kennsl á manninn</i>
<i>sem Janvier hitti á bryggjunni</i>

326
00:33:02,440 --> 00:33:05,760
<i>í Santa Monica.</i>
<i>Hann heitir Naseem Vaziri.</i>

327
00:33:05,840 --> 00:33:07,640
<i>Hann er Írani.</i>
<i>–Hann rekur alþjóðlegt</i>

328
00:33:07,720 --> 00:33:10,560
<i>farmflugsfyrirtæki og NSA</i>
<i>telur hann hafa náin tengsl</i>

329
00:33:10,640 --> 00:33:14,040
<i>við írönsku leyniþjónustuna.</i>
–Hvað ef Atley hefur þegar sagt

330
00:33:14,080 --> 00:33:19,320
Janvier nafn uppljóstrarans
í Íran? Janvier selur svo nafnið

331
00:33:19,360 --> 00:33:23,280
til Íranans Vaziris áður en hann
selur okkur Atley aftur.

332
00:33:23,320 --> 00:33:27,480
Janvier fær borgað frá báðum
og Cherokee lendir í háska.

333
00:33:27,560 --> 00:33:29,480
Það gæti þegar verið búið
að ná í Cherokee í Teheran.

334
00:33:29,560 --> 00:33:32,120
Þeir gætu verið að pynta hann núna.
–Eric, finndu út hvort Janvier

335
00:33:32,200 --> 00:33:35,360
hafi fengið einhverja peninga
inn á reikninginn sinn á síðustu

336
00:33:35,400 --> 00:33:37,640
tveimur sólarhringum.
–Það er of seint.

337
00:33:37,680 --> 00:33:40,200
Við verðum að halda áfram.
Við þurfum Atley til baka

338
00:33:40,280 --> 00:33:42,520
og þetta er verðið fyrir það.
–Verðið fyrir það?

339
00:33:42,600 --> 00:33:46,400
Þú þarft að hætta þessu.
–Hann drap okkar fólk! Okkar fólk!

340
00:33:46,480 --> 00:33:51,720
Tíminn er liðinn, Callen,
og við höfum ekki aðra áætlun.

341
00:33:56,040 --> 00:34:01,600
Mér þykir það leitt, herra Callen.
Mér þykir það leitt.

342
00:34:22,200 --> 00:34:26,800
Eintak af millifærslunni.
50 milljónir dala voru lagðar

343
00:34:26,880 --> 00:34:32,120
inn á reikninginn sem þú lést
okkur hafa á Cayman–eyjunum.

344
00:34:32,160 --> 00:34:34,520
Og bindandi samkomulag
sem lofar þér friðhelgi

345
00:34:34,600 --> 00:34:39,000
frá ákærum undirritað
af saksóknara Bandaríkjanna.

346
00:34:39,080 --> 00:34:42,280
Síma. 

347
00:34:46,440 --> 00:34:53,880
Og vatnsglas, takk fyrir.
–Náðu í vatn.

348
00:34:55,040 --> 00:35:02,480
Peningarnir eru komnir inn.
Ég bið um staðfestingu. 

349
00:35:10,360 --> 00:35:16,600
Já. Mjög gott.
Segðu þeim að koma með Atley.

350
00:35:34,520 --> 00:35:36,880
Ég á að gera.

351
00:35:40,800 --> 00:35:45,200
Beint áfram þar til ég gef þér
frekari leiðbeiningar.

352
00:35:45,280 --> 00:35:47,840
Hefurðu einhverja hugmynd um
hvert þeir eru að fara?

353
00:35:47,880 --> 00:35:53,160
Þeir eru á leið niður í miðbæ.
–Farðu til hægri.

354
00:35:56,120 --> 00:35:59,760
Hefur eitthvað meira verið lagt
inn á reikninginn hans?

355
00:35:59,840 --> 00:36:04,760
Ég er enn að vinna í því.
–Farðu næst til vinstri.

356
00:36:08,720 --> 00:36:13,320
Stoppaðu. 
Þetta er hér.

357
00:36:20,400 --> 00:36:26,000
Fréttaliðið er herkænskuaðgerð
til að halda þér hreinskilnum.

358
00:36:26,080 --> 00:36:31,920
Hvar er hann?
–Í jeppanum hinum megin við garðinn.

359
00:36:34,840 --> 00:36:39,120
Þú getur sent þitt fólk
að ná í hann.

360
00:36:52,760 --> 00:36:55,800
Er allt í lagi með þig?

361
00:36:56,880 --> 00:37:01,600
Ég er að leita að myndavélum.
Ég er með umferðarmyndavél.

362
00:37:01,760 --> 00:37:07,040
Þetta er brjálæði. –Já. 
Við getum ekki breytt þessu núna.

363
00:37:10,040 --> 00:37:13,360
Þetta er Atley.
–Hann er dópaður.

364
00:37:14,240 --> 00:37:16,960
Við erum komin inn.
Bankareikningur Janviers.

365
00:37:17,040 --> 00:37:18,680
Millifærsla snemma í gær.

366
00:37:18,760 --> 00:37:24,840
50 milljónir dala í gegnum banka
í Moskvu. –Hvaðan kom þetta?

367
00:37:25,600 --> 00:37:31,120
Teheran. –Svo herra Callen 
hafði rétt fyrir sér.

368
00:37:31,160 --> 00:37:34,560
Við getum ekki snúið aftur núna.

369
00:38:06,880 --> 00:38:10,080
Nei, þetta er ekki að gerast.
–Sam!

370
00:38:10,160 --> 00:38:14,880
Nei, G!
–Þú ferð hvergi.

371
00:38:15,200 --> 00:38:19,480
Ætlarðu að skjóta mig
fyrir framan allan heiminn?

372
00:38:23,480 --> 00:38:26,440
Náðu í útsendinguna.

373
00:38:29,440 --> 00:38:32,920
Maður getur aldrei farið
of varlega, Callen fulltrúi.

374
00:38:33,000 --> 00:38:36,240
G, ekki gera þetta.
Slepptu honum.

375
00:38:38,600 --> 00:38:41,800
Alríkisfulltrúar!
Við sjáum um þetta. Farið!

376
00:38:41,880 --> 00:38:43,920
Leggðu frá þér byssuna!
–Niður með vopnið!

377
00:38:43,960 --> 00:38:47,440
NCIS! 
Alríkisfulltrúar!

378
00:39:03,000 --> 00:39:06,480
Þinn leikur.

379
00:39:22,640 --> 00:39:24,480
Ekki skjóta! Ekki skjóta!
–Hann er með okkur.

380
00:39:24,560 --> 00:39:26,880
Niður með vopnið, G.
Niður með vopnið.

381
00:39:26,920 --> 00:39:29,720
Rólegur.
–Farðu upp að bílnum!

382
00:39:29,800 --> 00:39:32,800
Slakið á.
–Settu hendur fyrir aftan bak.

383
00:39:32,840 --> 00:39:35,720
Hvað sem þú segir er hægt
að nota gegn þér í réttarsal.

384
00:39:35,760 --> 00:39:40,160
Þú hefur rétt á lögfræðingi
sem er til staðar í yfirheyrslum.

385
00:39:40,200 --> 00:39:44,760
Ef þú hefur ekki efni á honum
muntu fá tilskipaðan...

386
00:41:11,560 --> 00:41:15,320
Íslenskur texti:
Eva Hjaltalín Ingólfsdóttir

387
00:41:15,400 --> 00:41:15,560
